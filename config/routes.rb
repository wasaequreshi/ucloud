Rails.application.routes.draw do
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'home#homepage'

  #home routes
  get '/homepage', :to=> 'home#homepage'
  get '/homepage/moveFirstFileGoogleToDropbox', :to => 'home#moveFirstFileGoogleToDropbox'
  get '/homepage/moveFirstFileDropboxToGoogle', :to => 'home#moveFirstFileDropboxToGoogle'
  get '/homepage/download', :to => 'home#download'
  post '/homepage/upload', :to => 'home#upload'

  #logins routes
  get '/login', :to => 'logins#new'
  post '/login', :to => 'logins#create'
  get '/logout', :to => 'logins#destroy'
  get 'logins/destroy'
  #signup routes
  get '/signup', :to => 'users#new'
  post '/signup', :to => 'users#create'

  #Authentication routes
  get   '/sessions/login', :to => 'sessions#new'
  get '/auth/:provider/callback', :to => 'sessions#create'
  get '/auth/failure', :to => 'sessions#failure'

  get '/homepage/get_google_file_url', :to => 'home#get_google_file_url'
  get '/homepage/get_dropbox_file_url', :to => 'home#get_dropbox_file_url'
  get '/homepage/downloadSplit', :to => 'home#downloadSplit'
  get '/homepage/uploadToDropbox', :to => 'home#uploadToDropbox'
  get '/homepage/uploadToGoogle', :to => 'home#uploadToGoogle'
  get '/homepage/uploadSplit', :to => 'home#uploadSplit'
  get '/homepage/uploadMirror', :to => 'home#uploadMirror'
  get '/homepage/swapLeftPane', :to => 'home#swapLeftPane'
  get '/homepage/swapRightPane', :to => 'home#swapRightPane'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
