class AddGoogleAuthsToUser < ActiveRecord::Migration
  def change
    add_column :users, :google_auths, :text
  end
end
