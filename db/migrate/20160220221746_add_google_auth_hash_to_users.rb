class AddGoogleAuthHashToUsers < ActiveRecord::Migration
  def change
    add_column :users, :google_auth_hash, :text
  end
end
