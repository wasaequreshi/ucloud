class AddDropboxAuthHashToUsers < ActiveRecord::Migration
  def change
    add_column :users, :dropbox_auth_hash, :text
  end
end
