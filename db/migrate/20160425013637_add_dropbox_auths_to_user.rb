class AddDropboxAuthsToUser < ActiveRecord::Migration
  def change
    add_column :users, :dropbox_auths, :text
  end
end
