class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :username
      t.string :password
      t.string :email
      t.text :google_token
      t.text :dropbox_token

      t.timestamps null: false
    end
  end
end
