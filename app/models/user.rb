class User < ActiveRecord::Base
	#attr_accessor :google_auth_hash, :dropbox_auth_hash
	before_save { self.email = email.downcase }
	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
	validates :email, presence: true, length: { maximum: 255 }, format: { with: VALID_EMAIL_REGEX }, uniqueness: true
	
	has_secure_password
	validates :password, presence: true, length: { minimum: 1 }, allow_nil: true
	
	serialize :google_auths, Array
	serialize :dropbox_auths, Array
	serialize :google_auth_hash, Hash
	serialize :dropbox_auth_hash, Hash

	def User.digest(string)
	    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
	                                                  BCrypt::Engine.cost
	    BCrypt::Password.create(string, cost: cost)
	end

	def add_google_account(auth)
		self.google_auths = [] if self.google_auths == nil
		update_attributes google_auths: self.google_auths + [auth]
	end

	def add_dropbox_account(auth)
		self.dropbox_auths = [] if self.dropbox_auths == nil
		update_attributes dropbox_auths: self.dropbox_auths + [auth]
	end

	def get_google_accounts
		return self.google_auths
	end

	def get_dropbox_accounts
		return self.dropbox_auths
	end

	def get_google_auth_hash
		return self.google_auth_hash
	end

	def get_dropbox_auth_hash
		return self.dropbox_auth_hash
	end

	def get_unique_accounts
		accounts = []
		for hash in self.google_auths
			accounts.push([hash['info']['email'], 'Google'])
		end
		for hash in self.dropbox_auths
			accounts.push([hash['info']['email'], 'Dropbox'])
		end
		if not self.google_auth_hash.nil? and not self.google_auth_hash.empty?
			accounts.push([self.google_auth_hash['info']['email'], self.google_auth_hash['provider'] == 'google_oauth2' ? 'Google' : 'Dropbox'])
		end
		if not self.dropbox_auth_hash.nil? and not self.dropbox_auth_hash.empty?
			accounts.push([self.dropbox_auth_hash['info']['email'], self.dropbox_auth_hash['provider'] == 'google_oauth2' ? 'Google' : 'Dropbox'])
		end

		return accounts.uniq
	end

	def swapLeftPane(email, provider)
		all_accounts = self.google_auths + self.dropbox_auths

		for account in all_accounts
			if account['info']['email'] == email and account['provider'] == provider
				update_attributes google_auth_hash: account
			end
		end
	end

	def swapRightPane(email, provider)
		all_accounts = self.google_auths + self.dropbox_auths

		for account in all_accounts
			if account['info']['email'] == email and account['provider'] == provider
				update_attributes dropbox_auth_hash: account
			end
		end
	end
end
