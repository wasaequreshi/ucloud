class Rest::GoogleRest < ActiveRecord::Base
	GOOGLE_CLIENT_ID = '863307608466-7fbfvlttcjuphtnp9tr1peuhmhg6mlh7.apps.googleusercontent.com'
	GOOGLE_CLIENT_SECRET = 'cQGBK7xXodZshHvAR-ZrjC8v'
	BASEURL = 'https://www.googleapis.com/drive/v2/'
	
	FOLDERNAME = "UCloud"
    MEGABYTE = 1024*1024
	@current_user
	@auth_hash

	def initialize(user, hash)
		@current_user = user
		@auth_hash = hash
	end

	def getFiles
		#@auth_hash = @current_user.google_auth_hash
		if (@auth_hash.nil? || @auth_hash.empty?)
		    return []
		end

		access_token = getAccessToken
		puts "GOT ACCESS TOKEN"
		#setup REST request
		url = BASEURL + 'files'
		headers = {
			'Authorization' => "Bearer " + access_token
		}

		#get files
		resp = HTTParty.get(url, :headers => headers)
		
		#get files
		files = []
		if resp.has_key?('items')
			resp['items'].each do |f|
				file = OpenStruct.new
				file.name = f['title']
				file.id = f['id']
				modified = f['modifiedDate'].split("T")[0]
				file.modified = modified
				file.provider = @auth_hash['provider']
				file.size = f['fileSize']
				file.downloadURL = f['webContentLink']
				files << file
			end
		end
		return files
	end

	def getFile(file)
		access_token = getAccessToken

		#setup rest call
		url = BASEURL + 'files/' + file.id
		params = {
			:alt => 'media'
		}
		headers = {
			'Authorization' => "Bearer " + access_token
		}

		#get file
		resp = HTTParty.get(url, :query => params, :headers => headers)

		#save file
		localFile = OpenStruct.new
		localFile.name = file.name
		localFile.content = resp.body.force_encoding("UTF-8")

		return localFile
	end
	
	def addFile(file)
		access_token = getAccessToken

        separator = "ucloud"
		#setup rest call
		url = 'https://www.googleapis.com/upload/drive/v2/files'
		params = {
			:uploadType => 'multipart'
		}
		headers = {
			'Authorization' => "Bearer " + access_token,
			'Content-Type' => "multipart/related; boundary=" + separator
		}
		
		#metadata upload
		body = {
		    :title => file.name
		}
		
		body = "\n--" + separator + "\nContent-Type: application/json\n\n" + body.to_json + 
		       "\n--" + separator + "\nContent-Type: application/octet-stream\n\n" + file.content + 
		       "\n--" + separator + "--\n"
		
		#add file
		resp = HTTParty.post(url, :query => params, :headers => headers, :body => body)
		
	end
	
	def addLarge(file)
	    Thread.new do
	        access_token = getAccessToken
	      
        	#setup rest call
        	url = 'https://www.googleapis.com/upload/drive/v3/files'
        	params = {
        		:uploadType => 'resumable'
        	}
        	headers = {
        		'Authorization' => "Bearer " + access_token,
        		'Content-Type' => "application/json; charset=UTF-8",
        		'X-Upload-Content-Length' => file.size.to_s,
        		'X-Upload-Content-Type' => "application/octet-stream"
        	}
        	
        	#metadata upload
        	body = {
        	    :name => file.name
        	}
		    #add file etadata and get resumable URI 
		    #See https://developers.google.com/drive/v3/web/manage-uploads#resumable
	    	resp = HTTParty.post(url, :query => params, :headers => headers, :body => body.to_json)
        	uploadId = resp.headers['Location']
        	
    	    #setup rest call
        	params = {
        		:uploadType => 'resumable',
        		:upload_id => uploadId
        	}
        	headers = {
        		'Authorization' => "Bearer " + access_token,
        		'Content-Length' => file.size.to_s
        	}
        	
        	#Get piece of file
        	chunk = file.content.read()
        	
        	#upload chunk by chunk upload
    	    resp = HTTParty.put(uploadId, :query => params, :headers => headers, :body => chunk)
    	    
    	    File.delete file.content.tempfile
	    end
	end
	
	def deleteFile(file)
		access_token = getAccessToken

		#setup rest call
		url = BASEURL + 'files/' + file.id
		headers = {
			'Authorization' => "Bearer " + access_token
		}

		#delete file
		resp = HTTParty.delete(url, :headers => headers)
				
		# puts resp
	end

	def get_file_downloadable_url(passed_in_file)
		check_and_update_access_token_expired
		files = getFiles
		for file in files
			# puts file.id
			# puts passed_in_file.id
			if (file.id == passed_in_file.id)
				# puts "FOUND!"
				return {downloadURL: file.downloadURL}
			end
		end
	end
	
	def check_and_update_access_token_expired
		expire_time = @auth_hash['credentials']['expire_time']
		current_time = Time.now
		
		if (current_time <=> expire_time) == 1
			refresh_access_token
		end
	end

	def refresh_access_token
		#@auth_hash = @current_user.google_auth_hash
		refresh_token = @auth_hash['credentials']['refresh_token']
		body = {
			'client_id' => GOOGLE_CLIENT_ID,
			'client_secret' => GOOGLE_CLIENT_SECRET,
			'refresh_token' => refresh_token,
			'grant_type' => 'refresh_token'
 		}
 		params = {
			'client_id' => GOOGLE_CLIENT_ID,
			'client_secret' => GOOGLE_CLIENT_SECRET,
			'refresh_token' => refresh_token,
			'grant_type' => 'refresh_token'
 		}
 		
 		headers = {
      		'Content-Type' => 'application/x-www-form-urlencoded'
    	}

    	resp = HTTParty.post('https://accounts.google.com/o/oauth2/token', :body => body, :params=>params.to_json, :headers => headers)

    	@auth_hash['credentials']['token'] = resp['access_token']
    	expire_time = Time.now + (60*60)
		@auth_hash['credentials']['expire_time'] = expire_time
		if @current_user.google_auth_hash['uid'] == @auth_hash['uid']
			@current_user.update(google_auth_hash: @auth_hash)
		elsif @current_user.dropbox_auth_hash['uid'] == @auth_hash['uid']
			@current_user.update(dropbox_auth_hash: @auth_hash)
		else
			puts "ERROR Line 213"
		end
	end
	
	def getspace
	    info = about
	    total = info['quotaBytesTotal'].to_i
	    used = info ['quotaBytesUsed'].to_i
		return (total - used) / 1048576
	end
	
	def about
		access_token = getAccessToken

		#setup rest call
		url = BASEURL + 'about'
		headers = {
			'Authorization' => "Bearer " + access_token
		}

		#get file
		resp = HTTParty.get(url, :headers => headers)

		#puts JSON.pretty_generate resp
		return resp
	end
	
	def getAccessToken
		#@auth_hash = @current_user.google_auth_hash
		check_and_update_access_token_expired
		# puts @current_user.to_json
		return @auth_hash["credentials"]["token"]
	end

	def getEmail
		#@auth_hash = @current_user.google_auth_hash
		return @auth_hash['info']['email']
	end

	def getProviderType(s)
		if s == "google_oauth2"
			return "Google"
		else
			return "Dropbox"
		end 
	end

end
