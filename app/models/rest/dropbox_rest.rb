class Rest::DropboxRest < ActiveRecord::Base

	BASEURL = 'https://api.dropboxapi.com/2/'
	BASEPOSTURL = 'https://content.dropboxapi.com/2/'
    MEGABYTE = 1024*1024
	@current_user
	@auth_hash

	def initialize(user, hash)
		@current_user = user
		@auth_hash = hash
	end

	def getFiles
		#auth_hash = @current_user.dropbox_auth_hash
		#Check if nil because it should show no files if no account has been attached
		if (@auth_hash.nil? || @auth_hash.empty?)
		    return []
		end
		access_token = getAccessToken

		#setup rest call
		link = BASEURL + 'files/list_folder'
		params = {
		    :path => ""
		}
		headers = {
			'Authorization' => "Bearer " + access_token,
			'Content-Type' => 'application/json'
		}

		resp = HTTParty.post(link, :body => params.to_json, :headers => headers)

		#set files
		files = []
		if resp.has_key?('entries')
			resp['entries'].each do |f|
				file = OpenStruct.new
				file.name = f['name']
				file.id = f['id']
				file.path = f['path_display']
                if f['client_modified'] == nil
                    modified = ""
                    file.modified = ""
                else
                    modified = f['client_modified'].split("T")[0]
                    file.modified = modified
                end
                file.size = f['size']
				file.provider = @auth_hash['provider']
				files << file
			end
		end

		#puts resp

		return files
	end
	
	def getFile(file)
		access_token = getAccessToken

		#setup rest call
		link = BASEPOSTURL + 'files/download'
		params = {
		    :path => file.id
		}
		headers = {
			'Authorization' => "Bearer " + access_token,
			'Content-Type' => '',
			'Dropbox-API-arg' => params.to_json
		}
		
		#get file
		resp = HTTParty.post(link, :headers => headers)
		
		#save file
		localFile = OpenStruct.new
		localFile.name = file.name
		localFile.content = resp.force_encoding("UTF-8")
		
		return localFile
	end

	def addFile(file)
		access_token = getAccessToken

		#setup rest call
		link = BASEPOSTURL + 'files/upload'
		params = {
		    :path => '/' + file.name
		}
		headers = {
			'Authorization' => "Bearer " + access_token,
			'Content-Type' => 'application/octet-stream',
			'Dropbox-API-arg' => params.to_json
		}
		puts "THE FILE CONTENT"
		#puts file.content
		#upload file
		resp = HTTParty.post(link, :headers => headers, :body => file.content)
		
		puts "--- UPLOAD RESPONSE ---"
		#puts resp
	end

    def addLarge(file)
	    puts "got to large upload"
	    Thread.new do
	        access_token = getAccessToken
	        
	        #initialize session
		    link = BASEPOSTURL + 'files/upload_session/start'
		    
    		params = {
    		    :close => 'true'
    		}
		    
    		headers = {
    			'Authorization' => "Bearer " + access_token,
    			'Content-Type' => 'application/octet-stream'
    			#'Dropbox-API-arg' => params.to_json
    		}
	        
    		resp = HTTParty.post(link, :headers => headers)
    		
    		puts "--- UPLOAD RESPONSE ---"
    		puts resp
        	uploadId = resp['session_id']
    		
    		offset = 0
    		
    		#upload parts
    		until file.content.eof?
        	    #setup rest call
            	url = BASEPOSTURL + 'files/upload_session/append_v2'
            	params = {
    		        :cursor => {:session_id => uploadId, :offset => offset * 30 * MEGABYTE}
            	}
            	headers = {
    			    'Authorization' => "Bearer " + access_token,
    			    'Content-Type' => 'application/octet-stream',
    			    'Dropbox-API-arg' => params.to_json
            	}
            	
            	#Get piece of file
            	chunk = file.content.read(30 * MEGABYTE)
            	offset+=1
            	
            	#upload chunk by chunk upload
	    	    resp = HTTParty.post(url, :headers => headers, :body => chunk)
	    	    puts "--chunk--"
	    	    puts resp.to_json
	    	end
    		
    		
    		#end session
		    link = BASEPOSTURL + 'files/upload_session/finish'
		    
    		params = {
    		    :cursor => {:session_id => uploadId, :offset => file.size},
    		    :commit => {:path => '/' + file.name}
    		}
		    
    		headers = {
    			'Authorization' => "Bearer " + access_token,
    			'Content-Type' => 'application/octet-stream',
    			'Dropbox-API-arg' => params.to_json
    		}
	        
    		resp = HTTParty.post(link, :headers => headers)
    		
    		puts "--- FINISH RESPONSE ---"
    		puts resp
        	uploadId = resp['session_id']
        	
	    	File.delete file.content.tempfile
        end
        
    end

	def deleteFile(file)
		access_token = getAccessToken

		#setup rest call
		link = BASEURL + 'files/delete'
		params = {
		    :path => '/' + file.name
		}
		headers = {
			'Authorization' => "Bearer " + access_token,
			'Content-Type' => "application/json"
		}
		
		#make rest call
		resp = HTTParty.post(link, :body => params.to_json, :headers => headers)
		
		puts "--- DELETE RESPONSE ---"
	end

	def get_file_downloadable_url(passed_in_file)
		
		localFile = getFile passed_in_file

		access_token = getAccessToken
		link = "https://api.dropboxapi.com/2/sharing/list_shared_links"
		params = {
			'path' => '/' + localFile.name
		}
		headers = {
			'Authorization' => "Bearer " + access_token,
			'Content-Type' => 'application/json'
		}

		resp = HTTParty.post(link, :params => params.to_json, :headers => headers, body: params.to_json)
		
		arrayCheck = resp['links'][0]
		
		if (resp['links'].empty? || arrayCheck.nil?)
			link = "https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings"
			resp = HTTParty.post(link, :params => params.to_json, :headers => headers, body: params.to_json)
			final_url = resp['url']
		else
			final_url = resp['links'][0]['url']
		end
		return {downloadURL: final_url}
		
	end
	
	def getspace
		access_token = getAccessToken
		
		url = BASEURL + 'users/get_space_usage'
		headers = {
			'Authorization' => "Bearer " + access_token,
			'Content-Type' => 'application/json'
		}
		
		resp = HTTParty.post(url, :headers => headers, :body => nil.to_json)
		
		total = resp['allocation']['allocated']
		used = resp['used']
		
		#return available in MB
		return (total - used) / 1048576
	end

	def getEmail
		return @auth_hash['info']['email']
	end
	
	def getAccessToken
		return @auth_hash["credentials"]["token"]
	end

	def getProviderType(s)
		if s == "google_oauth2"
			return "Google"
		else
			return "Dropbox"
		end 
	end
end
