class SessionsController < ApplicationController
	@GOOGLE_CLIENT_ID = '863307608466-7fbfvlttcjuphtnp9tr1peuhmhg6mlh7.apps.googleusercontent.com'
	@GOOGLE_CLIENT_SECRET = 'cQGBK7xXodZshHvAR-ZrjC8v'
	@GOOGLE_ENDPOINT = 'https://www.googleapis.com/oauth2/v4/token/'

	def new
	end

	def create
		auth_hash = request.env['omniauth.auth']
		provider = auth_hash['provider']
		current_user
		# get the current user, saved in @current_user - helpers/logins_helper
		puts "IN SESSION CONTROLLER"
		#puts request.to_json
		puts "CURRENT USER: " + @current_user.to_json
		if provider == 'google_oauth2'
			expire_time = Time.now + (60*60)
			auth_hash['credentials']['expire_time'] = expire_time
			@current_user.add_google_account(auth_hash)
			#Remove after everything is refactoring
			@current_user.update(google_auth_hash: auth_hash)
		elsif provider == 'dropbox_oauth2'
			@current_user.add_dropbox_account(auth_hash)
			#Remove after everything is refactoring
			@current_user.update(dropbox_auth_hash: auth_hash)
		end
		
		redirect_to root_url
	end

	def failure
		puts "ERR"
	end
end
