class HomeController < ApplicationController
    MEGABYTE = 1024*1024
	@@leftPane = nil
	@@rightPane = nil
	@@tempfile
	@@content = OpenStruct.new
	skip_before_filter :verify_authenticity_token

	def homepage
		if not logged_in?
			redirect_to controller: 'logins', action: 'new'
			return
		end
		current_user

		initLeftPane
		initRightPane
		@googleFiles = @@leftPane.getFiles

		@dropboxFiles = @@rightPane.getFiles

		if !@current_user.dropbox_auth_hash.nil? and !@current_user.dropbox_auth_hash.empty?
		    @dSpace = @@rightPane.getspace
		    @demail = @@rightPane.getEmail
		    @dbprovider = "Dropbox"
		    @total_accounts_google = @current_user.get_unique_accounts
		   	@dropbox_accounts = @total_accounts_google.uniq

		end
		if !@current_user.google_auth_hash.nil? and !@current_user.google_auth_hash.empty?
		    @gSpace = @@leftPane.getspace
		    @gemail = @@leftPane.getEmail
		    @gprovider = "Google"
		    @total_accounts_dropbox = @current_user.get_unique_accounts
		    @google_accounts = @total_accounts_dropbox.uniq
		end
	end

	def initLeftPane
		if @current_user.google_auth_hash == nil or @current_user.google_auth_hash.empty?
			@@leftPane = Rest::DropboxRest.new(@current_user, {})
		elsif @current_user.google_auth_hash['provider'] == "dropbox_oauth2"
			@@leftPane = Rest::DropboxRest.new(@current_user, @current_user.google_auth_hash)
		else
			@@leftPane = Rest::GoogleRest.new(@current_user, @current_user.google_auth_hash)
		end
	end

	def initRightPane
		if @current_user.dropbox_auth_hash == nil or @current_user.dropbox_auth_hash.empty?
			@@rightPane = Rest::GoogleRest.new(@current_user, {})
		elsif @current_user.dropbox_auth_hash['provider'] == "google_oauth2"
			@@rightPane = Rest::GoogleRest.new(@current_user, @current_user.dropbox_auth_hash)
		else
			@@rightPane = Rest::DropboxRest.new(@current_user, @current_user.dropbox_auth_hash)
		end

	end

	def moveFirstFileGoogleToDropbox
		name = params[:name]
		id = params[:id]
		googleFile = OpenStruct.new
		googleFile.name = name
		googleFile.id = id

		#download google file
		localFile = @@leftPane.getFile googleFile

		#push file to dropbox
		@@rightPane.addFile localFile

		#delete google file
		@@leftPane.deleteFile googleFile

		#delete local file
		#File.delete localFile

		redirect_to '/homepage'
	end

	def moveFirstFileDropboxToGoogle
		name = params[:name]
		id = params[:id]
		dropboxFile = OpenStruct.new
		dropboxFile.name = name
		dropboxFile.id = id

		#download google file
		localFile = @@rightPane.getFile dropboxFile

		#push file to dropbox
		@@leftPane.addFile localFile

		#delete dropbox file
		@@rightPane.deleteFile dropboxFile

		redirect_to '/homepage'
	end

	def uploadToDropbox

    	file = OpenStruct.new
		if @@tempFile.size > 80 * MEGABYTE
    		file.content = @@tempFile
            file.name = @@tempFile.original_filename
            file.size = @@tempFile.size
            @@rightPane.addLarge file
		else
    		file.content = @@tempFile.read
            file.name = @@tempFile.original_filename

            #delete tempfile
            File.delete @@tempFile.tempfile

            #upload file
            @@rightPane.addFile file
        end
        render nothing: true
	end

	def uploadToGoogle
		file = OpenStruct.new

		if @@tempFile.size >  80 * MEGABYTE
    		file.content = @@tempFile
            file.name = @@tempFile.original_filename
            file.size = @@tempFile.size

            @@leftPane.addLarge file
		else
    		file.content = @@tempFile.read
            file.name = @@tempFile.original_filename

            #delete tempfile
            File.delete @@tempFile.tempfile

            #upload file
            @@leftPane.addFile file
        end

        render nothing: true
	end

	def upload
	    #create our file object with file name and content
        @@tempFile = params[:file]
        render nothing: true
    end

	def get_google_file_url
		name = params[:name]
		id = params[:id]
		googleFile = OpenStruct.new
		googleFile.name = name
		googleFile.id = id

		#download google file
		@@content = @@leftPane.getFile googleFile
		localFile = {}
		respond_to do |format|
      	format.html
      	format.json {render json: localFile }
      	end
	end

	def downloadSplit
	   filename = params[:name]
	   files1 = @@leftPane.getFiles
	   files2 = @@rightPane.getFiles
	   f1 = nil
	   f2 = nil
	   thread1Pane = nil
	   thread2Pane = nil
	   name = filename.split(/\-\d\-ucloud/).first.strip
	   files1.each do |f|
	       if (f2.nil? and f.name.include?('-1-ucloud') and f.name.include?(name))
	           thread1Pane = @@leftPane
	           f1 = f
	       end
	       if (f1.nil? and f.name.include?('-2-ucloud') and f.name.include?(name))
	           thread2Pane = @@leftPane
	           f2 = f
	       end
	   end
	   files2.each do |f|
	       if (f1.nil? and f.name.include?('-1-ucloud') and f.name.include?(name))
	           thread1Pane = @@rightPane
	           f1 = f
	       end
	       if (f2.nil? and f.name.include?('-2-ucloud') and f.name.include?(name))
	           thread2Pane = @@rightPane
	           f2 = f
	       end
	   end

	   thread1 = Thread.new {
	   		f1 = thread1Pane.getFile f1
	   }
	   thread2 = Thread.new {
	   		f2 = thread2Pane.getFile f2
	   }

	   thread1.join
	   thread2.join
	   @@content.content = f1.content + f2.content
	   @@content.name = name

    	respond_to do |format|
      	format.html
      	format.json {render json: {} }
      	end
	end

	def download
        send_data @@content.content, :filename => @@content.name.strip
	end

	def get_dropbox_file_url
		name = params[:name]
		id = params[:id]

		dropboxFile = OpenStruct.new
		dropboxFile.name = name
		dropboxFile.id = id

		#download google file
		@@content = @@rightPane.getFile dropboxFile
		localFile = {}
		respond_to do |format|
      	format.html
      	format.json {render json: localFile }
      	end
	end

	def uploadSplit
		firstHalf = OpenStruct.new
 		firstHalf.content = @@tempFile.read(@@tempFile.size/2)
 		firstHalf.size = @@tempFile.size/2
 		firstHalf.name = @@tempFile.original_filename + "-1-ucloud.txt"

		secondHalf = OpenStruct.new
 		secondHalf.content = @@tempFile.read()
 		secondHalf.size = @@tempFile.size - firstHalf.size
 		secondHalf.name = @@tempFile.original_filename + "-2-ucloud.txt"

		# first half goes to right pane
		rightUploadThread = Thread.new {
			# puts "\nuploading to dropbox...\n"
			if firstHalf.size > 80 * MEGABYTE
        @@rightPane.addLarge firstHalf
			else
        @@rightPane.addFile firstHalf
      end
      # puts "\nfinished uploading to right\n"
	}


    # second half goes to left pane
    leftUploadThread = Thread.new {
			# puts "\nuploading to google...\n"
			if secondHalf.size >  80 * MEGABYTE
        @@leftPane.addLarge secondHalf
			else
        @@leftPane.addFile secondHalf
      end
      # puts "\nfinished uploading to left\n"
    }


    rightUploadThread.join
    leftUploadThread.join

    File.delete @@tempFile.tempfile
    render nothing: true
  end

	def uploadMirror
		firstHalf = OpenStruct.new
 		firstHalf.content = @@tempFile.read(@@tempFile.size/2)
 		firstHalf.size = @@tempFile.size/2
 		firstHalf.name = @@tempFile.original_filename + "-1-ucloud.txt"

		secondHalf = OpenStruct.new
 		secondHalf.content = @@tempFile.read()
 		secondHalf.size = @@tempFile.size - firstHalf.size
 		secondHalf.name = @@tempFile.original_filename + "-2-ucloud.txt"

		# first half going to right pane
		rightUploadThread1 = Thread.new {
			# puts "\nuploading to dropbox...\n"
			if firstHalf.size > 80 * MEGABYTE
        @@rightPane.addLarge firstHalf
			else
        @@rightPane.addFile firstHalf
      end
      # puts "\nfinished uploading first half to right\n"
		}

		# second half going to right pane
		rightUploadThread2 = Thread.new {
			# puts "\nuploading to dropbox...\n"
			if secondHalf.size > 80 * MEGABYTE
        @@rightPane.addLarge secondHalf
			else
        @@rightPane.addFile secondHalf
      end
      # puts "\nfinished uploading second half to right to right\n"
	}

    # first half going to left pane
    leftUploadThread1 = Thread.new {
			# puts "\nuploading to google...\n"
			if firstHalf.size >  80 * MEGABYTE
        @@leftPane.addLarge firstHalf
			else
        @@leftPane.addFile firstHalf
      end
      # puts "\nfinished uploading first half to left\n"
    }

    # second half going to left pane
    leftUploadThread2 = Thread.new {
			# puts "\nuploading to google...\n"
			if secondHalf.size >  80 * MEGABYTE
        @@leftPane.addLarge secondHalf
			else
        @@leftPane.addFile secondHalf
      end
      # puts "\nfinished uploading second half to left\n"
    }

    rightUploadThread1.join
    rightUploadThread2.join
    leftUploadThread1.join
    leftUploadThread2.join

    File.delete @@tempFile.tempfile
    render nothing: true
  end

	def swapLeftPane

		email = params[:emailToSwap]
		provider = params[:provided]
		prov = ''

		current_user

		if provider == "Google"
			prov = "google_oauth2"
		else
			prov = "dropbox_oauth2"
		end

		@current_user.swapLeftPane(email, prov)

		redirect_to '/homepage'
	end

	def swapRightPane

		email = params[:emailToSwap]
		provider = params[:provided]
		prov = ''

		current_user

		if provider == "Google"
			prov = "google_oauth2"
		else
			prov = "dropbox_oauth2"
		end

		@current_user.swapRightPane(email, prov)

		redirect_to '/homepage'
	end
end
