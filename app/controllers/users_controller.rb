class UsersController < ApplicationController
	def show
    	@user = User.find(params[:id])
  	end

	def new
		@user = User.new
	end

	def create
		@user = User.new(user_params)
		if @user.save
	    	log_in @user
	    	redirect_to controller: 'home', action: 'homepage'
	    else
	    	render 'new'
	    end
	end

	def destroy
	end

	private

	    def user_params
	      params.require(:user).permit(:username, :email, :password,
	                                   :password_confirmation)
	    end
end
