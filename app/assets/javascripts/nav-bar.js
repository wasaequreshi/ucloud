$(function(){
  $(".onedrive-text").css({ 'opacity' : 0.5 });
  $(".upload-div").hide();

  google_navbar_setup();
  dropbox_navbar_setup();
  onedrive_navbar_setup();
  upload_navbar_setup();
  logout_navbar_setup();
});
googleDown = false;
dropboxDown = false;

var upload_navbar_setup = function()
{
    $('.upload-text').click(function(e) {
      //Displaying files
      unhighlight_all();
      $(".upload-div").show();

      $(".upload-text").css({ 'opacity' : 0.5 });
    }).mouseover(function(){ $(this).addClass("a_hand") }); //Hand!
}

var logout_navbar_setup = function()
{
    $('.logout-text').click(function(e) {
      //Displaying files
      unhighlight_all();
      $(".logout-text").css({ 'opacity' : 0.5 });
     
    }).mouseover(function(){ $(this).addClass("a_hand") }); //Hand!
}

var onedrive_navbar_setup = function()
{
    $('.onedrive-text').click(function(e) {
      //Displaying files
      unhighlight_all();
      $(".onedrive-text").css({ 'opacity' : 0.5 });
      $("#google-files").show();
      $("#dropbox-files").show();
    }).mouseover(function(){ $(this).addClass("a_hand") }); //Hand!
}
var google_navbar_setup = function()
{
    $(".Google-Div ul").hide();
    // $("#google-files").hide();
    $('.Google-Div ul').on('click','.sub-accounts',function(e){
      e.stopPropagation();
    });
     $('.Google-Div li').on('click','.sub-accounts',function(e){
      e.stopPropagation();
    });
    $('.Google-Div button').on('click','.sub-accounts',function(e){
      e.stopPropagation();
    });

    $('.Google-Div').click(function(e) {
      if (e.target.innerHTML == "Google")
      {
        unhighlight_all();
        $(this).children('ul').slideToggle(200); //Hides if shown, shows if hidden
        if (googleDown)
        {
            googleDown = false;
            $(".Google-text").css({ 'opacity' : 1.0 });
            $("#google-files").hide();
        } 
        else
        {
            $("#google-files").show();
            $(".Google-text").css({ 'opacity' : 0.5 });
            googleDown = true;
        } 
      }
  
    }).mouseover(function(){ $(this).addClass("a_hand") }); //Hand!
}

var dropbox_navbar_setup = function()
{
    //Dropdown menu for dropbox
    // $("#dropbox-files").hide();
    $(".Dropbox-Div ul").hide();
    $('.Dropbox-Div ul').on('click','.sub-accounts',function(e){
      e.stopPropagation();
    });
     $('.Dropbox-Div li').on('click','.sub-accounts',function(e){
      e.stopPropagation();
    });
    $('.Dropbox-Div button').on('click','.sub-accounts',function(e){
      e.stopPropagation();
    });
     $('.Dropbox-Div').click(function(e) {
      if (e.target.innerHTML == "Dropbox")
      {
        unhighlight_all();
        $(this).children('ul').slideToggle(200); //Hides if shown, shows if hidden
         if (dropboxDown)
        {
             $("#dropbox-files").hide();
              dropboxDown = false;
            $(".dropbox-text").css({ 'opacity' : 1.0 });
        } 
        else
        {
          $("#dropbox-files").show();
            $(".dropbox-text").css({ 'opacity' : 0.5 });
            dropboxDown = true;
        } 
      }
   
    }).mouseover(function(){ $(this).addClass("a_hand") }); //Hand!
}
var unhighlight_all = function()
{
    $("#google-files").hide();
    $("#dropbox-files").hide();
    $(".upload-div").hide();
    googleDown = false;
    dropboxDown =false;
    $(".dropbox-text").css({ 'opacity' : 1.0 });
    $(".Google-text").css({ 'opacity' : 1.0 });
    $(".onedrive-text").css({ 'opacity' : 1.0 });
    $(".logout-text").css({ 'opacity' : 1.0 });
    $(".upload-text").css({ 'opacity' : 1.0 });

}