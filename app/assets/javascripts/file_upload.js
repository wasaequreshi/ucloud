$(document).ready(function() {
	Dropzone.options.myDropzone = {
		maxFilesize: 2000,
		init: function() {
			this.on("uploadprogress", function(file, progress) {
				console.log("File progress", progress);
			});
		}
	}
});

var upload_file = function()
{
	var selected_option = $("#account-option-upload option:selected").text();
	if (selected_option.indexOf("Dropbox") >= 0)
	{
		$.ajax({
            type: 'GET',
            url: '/homepage/uploadToDropbox'
          });
		window.location.reload();
	}
	else if (selected_option.indexOf("Google") >= 0)
	{
		$.ajax({
            type: 'GET',
            url: '/homepage/uploadToGoogle'
          });
		window.location.reload();
	}
	else if (selected_option.indexOf("split") >= 0)
	{
		$.ajax({
            type: 'GET',
            url: '/homepage/uploadSplit'
          });
		window.location.reload();
	}
	else if (selected_option.indexOf("mirror") >= 0)
	{
		$.ajax({
            type: 'GET',
            url: '/homepage/uploadMirror'
          });
		window.location.reload();
	}
}

var leftpane_switch_account = function()
{
    var email = $("#account-option-google option:selected").text().split(" ")[0];
    var provider = $("#account-option-google option:selected").text().split(" ")[2];
    console.log(email);
    console.log(provider);
    $.ajax({
        type: 'GET',
        url: '/homepage/swapLeftPane',
        data: { emailToSwap : email, provided: provider },
        success: function(data)
        {
            //console.log("Success");
        }

    });

}
var rightpane_switch_account = function()
{
    var email = $("#account-option-dropbox option:selected").text().split(" ")[0];
    var provider = $("#account-option-dropbox option:selected").text().split(" ")[2];
    console.log(email);
    console.log(provider);
    $.ajax({
        type: 'GET',
        url: '/homepage/swapRightPane',
        data: { emailToSwap : email, provided: provider },
        success: function(data)
        {
            //console.log("Success");

        }
    });
}