toastr.options = {
  "closeButton": false,
  "debug": false,
  "newestOnTop": false,
  "progressBar": false,
  "positionClass": "toast-bottom-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}

$(function() {
  $( "#g-files, #db-files" ).sortable({
      receive: function(event, ui) {
      move_files(ui);
  },
    connectWith: ".connectedSortable"
  }).disableSelection();
   
});

 $(function() {
    $.contextMenu({
        selector: '.ui-sortable-handle', 
        callback: function(key, options) {
            context_menu_action(key, options);
            var m = "clicked: " + key + " " + options;
          //  console.log(options);
            // window.console && console.log(m) || alert(m); 
        },
        items: {
            "Download": {name: "Download File"},
            "Rename": {name: "Rename File"},
            "Download Split": {name: "Download Split"}
        }
    });

    $('.ui-sortable-handle').on('click', function(e){
        console.log('clicked', this);
    })    
});

var download_db_file = function(file_name, file_id, file_name)
{
 // console.log("Downloading DB File...");
  if (file_class.indexOf("Dropbox") >= 0)
  {
      $.ajax({
        type: 'GET',
        url: '/homepage/get_dropbox_file_url',
         data: { name : file_name, id : file_id }, 
         dataType: "json",
         success: function(data)
         {
            window.location = 'homepage/download';

        }
      });
      toastr.success('Downloading...');
      
  }

}
var download_google_file = function (file_name, file_id, file_name)
{
//  console.log("Downloading google File...");
  if (file_class.indexOf("Google") >= 0)
  {
       $.ajax({
        type: 'GET',
        url: '/homepage/get_google_file_url',
         data: { name : file_name, id : file_id },
         dataType: "json",
         success: function(data)
         {
            window.location = 'homepage/download';
         } 
      });
      toastr.success('Downloading...');
     
  }
}

var download_split = function (file_name, file_id)
{
    $.ajax({
        type: 'GET',
        url: '/homepage/downloadSplit',
         data: { name : file_name, id : file_id },
         dataType: "json",
         success: function(data)
         {
            window.location = 'homepage/download';
         } 
      });
    toastr.success('Downloading...');
     
}

var context_menu_action = function(key, options)
{
    if (key == "Download")
    {
        element = options["$trigger"];
        file_name = element.text();
        file_name = file_name.split("|")[0].trim();
        file_id = element.attr('id');
        file_class = element.attr('class');
        parent = element.parent().attr('id');
  
        if (parent == "g-files")
        {
          download_google_file(file_name, file_id, file_name);
        }
        else if (parent == "db-files")
        {
          download_db_file(file_name, file_id, file_name);
        }

    }
    else if (key == "Rename")
    {
       console.log("Not implemented yet");
    }
    else if (key == "Download Split")
    {
        element = options["$trigger"];
        file_name = element.text();
        file_id = element.attr('id');
        file_class = element.attr('class');
        parent = element.parent().attr('id');
        download_split(file_name, file_id);
    }
}

var move_files = function(ui)
{
    element = ui.item;
        
    item_moved = ui.item.attr('class');
    file_name = ui.item.text();
    file_name = file_name.split("|")[0].trim();
    file_id = ui.item.attr('id');
    file_class = ui.item.attr('class');
    parent = ui.item.parent().attr('id');
  
    if (parent == "g-files")
    {
        dropbox_to_google(file_name, file_id, file_class);
    }
    else if (parent == "db-files")
    {
        google_to_dropbox(file_name, file_id, file_class);
    }
    else
    {
        console.log("Not a valid parent");
    }
}

var google_to_dropbox = function(file_name, file_id, file_class)
{
    if (file_class.indexOf("Dropbox") < 0)
      {
           $.ajax({
            type: 'GET',
            url: '/homepage/moveFirstFileGoogleToDropbox',
             data: { name : file_name, id : file_id } 
          });
          toastr.success('Successfully moved file to Dropbox');
         
      }
}

var dropbox_to_google = function(file_name, file_id, file_class)
{
  if (file_class.indexOf("Google") < 0)
  {
      $.ajax({
        type: 'GET',
        url: '/homepage/moveFirstFileDropboxToGoogle',
         data: { name : file_name, id : file_id } 
      });
      toastr.success('Successfully moved file to Google');
      
  }
}

